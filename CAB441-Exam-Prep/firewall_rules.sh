#!/bin/bash

###Practical 3's Rules for the external router.

## Drop all packets by default
iptables --policy INPUT DROP
iptables --policy OUTPUT DROP
iptables --policy FORWARD DROP

##Setup the outgoing packets to have the public ip address.
iptables -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE

##Set up port forwarding for incomming packets, and allow established or related packets through
iptables -A FORWARD -i enp0s3 -o enp0s8 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
#or
#iptables -A FORWARD -m state --state RELATER,ESTABLISHED -j ACCEPT

##Use DNAT to change incomming port 80 traffic destination IP address to the web server
iptables -t nat -A PREROUTING -i enp0s3 -p tcp -m tcp --dport 80 -j DNAT --to-destination 192.168.1.80:80
iptables -t nat -A PREROUTING -i enp0s3 -p tcp -m tcp --dport 443 -j DNAT --to-destination 192.168.1.80:443

##Use SNAT to change outgoing source IP address to the firewalls IP address for web traffic
iptables -t nat -A POSTROUTING -p tcp -m tcp --dport 80 -j SNAT --to-source 10.0.2.15
iptables -t nat -A POSTROUTING -p tcp -m tcp --dport 443 -j SNAT --to-source 10.0.2.15

##Set up an iptable rule/s to send web traffic to the web proxy
#since the standard way no longer works we have to mangle the packet to be routed by a different
#table that only has a default route to the squid server

#permit squid box out to the internet
iptables -t mangle -A PREROUTING -p tcp --dport 80 -s 192.168.1.1 -j ACCEPT
# mark every every packet on port 80 to be routerd to the squid box
iptables -t mangle -A PREROUTING -i enp0s3 -p tcp --dport 80 -j MARK --set-mark 2 #mark packets
iptables -t mangle -A PREROUTING -m mark --mark 2 -j ACCEPT # allow marked packets

# Route the marked packets to the proxy without chagning the DST address
ip rule add fwmark 2 table 201
ip route add default via 192.168.1.1 table 201

## Internal router rules for the transparent router
iptables -t nat -A PREROUTING -s 192.168.1.1 -p tcp --dport 80 -j ACCEPT # allow web packets
iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080 # redirect the port to the squid port
iptables -t nat -A POSTTROUTING -p tcp --dport 80 -j MASQUERADE
iptables -t mangle -A PREROUTING -p tcp --dport 8080 -j DROP

###
### Practical 4
###

## Drop all packets by default
#iptables --policy INPUT DROP
#iptables --policy OUTPUT DROP
#iptables --policy FORWARD DROP

## Allow packets for existing or related connections through
#iptables -A FORWARD -i enp0s3 -o enp0s8 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
#or
#iptables -A FORWARD -m state --state RELATER,ESTABLISHED -j ACCEPT

## Allow udp traffic on port 1194 through and forward the traffic to the vpn server
iptables -t filter -A INPUT -p udp -m udp --dport 1194 -j ACCEPT
iptables -t nat -A PREROUTING -i enp0s3 -p tcp -m tcp --dport 1194 -j DNAT --to-destination 192.168.1.1:1194
iptables -t nat -A POSTROUTING -p tcp -m tcp --dport 1194 -j SNAT --to-source 10.0.2.15

## VPN Server rules
#Allow tunnel traffic on the INPUT OUTPUT and FORWARD chains
iptables -t filter -A INPUT -i tun0 -j ACCEPT
iptables -t filter -A OUTPUT -o tun0 -j ACCEPT
iptables -t filter -A FORWARD -o tun0 -j ACCEPT
iptables -t filter -A FORWARD -i tun0 -j ACCEPT

#masquerade the tunnel traffic
iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o enp0s3 -j MASQUERADE


###
### PRacticle 5
###

#Dmz firewall rules
#allow the internal netword access to the ladp authentication service
iptables -t filter -A INPUT -s 10.10.1.0/24 -p tcp -m tcp --dport 389 -j ACCEPT
iptables -t filter -A INPUT -s 192.168.1.1 -p tcp -m tcp --dport 389 -j ACCEPT
iptables -t filter -A INPUT -s 10.10.1.0/24 -p tcp -m tcp --dport 636 -j ACCEPT
iptables -t filter -A INPUT -s 192.168.1.1 -p tcp -m tcp --dport 636 -j ACCEPT
