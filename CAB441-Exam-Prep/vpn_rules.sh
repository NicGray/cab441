#!/bin/bash
# This script sets up the iptables rules to allow the 

# DROP policy on all chains
iptables --policy INPUT DROP
iptables --policy OUTPUT DROP
iptables --policy FORWARD DROP

#Allow DNS connections into the server
#iptables -A OUTPUT -d 255.255.255.255 -j ACCEPT
#iptables -A INPUT -s 255.255.255.255 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 53 -j ACCEPT
iptables -A OUTPUT -p udp -m udp --dport 53 -j ACCEPT

# Let packets for existing or related connections through
iptables -t filter -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A OUTPUT -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT

# Allow udp traffic on port 1194
iptables -t filter -A INPUT -p udp -m udp --dport 1194 -j ACCEPT

# Allow tunnel inteface of INPUT, OUTPUT and FORWARD
iptables -t filter -A INPUT -i tun0 -j ACCEPT
iptables -t filter -A OUTPUT -o tun0 -j ACCEPT
iptables -t filter -A FORWARD -i tun0 -j ACCEPT
iptables -t filter -A FORWARD -o tun0 -j ACCEPT

iptables -t nat -A POSTROUTING -s 10.8.0.0/24 -o enp0s3 -j MASQUERADE
#iptables -t filter -A INPUT -p udp -m udp --dport 1194 -j ACCEPT
